import * as fs from 'fs';
import Optionator from 'optionator';
import * as path from 'path';
import { Servr } from './app';
import { CLIOptions } from './constants/cli-options.constant';
import { Logger } from './lib/log';

const optionator = Optionator(CLIOptions);

/** Delegate CLI commands to appropriate logic */
export async function cli(argz: any): Promise<void> {
  const options = optionator.parseArgv(argz);
  const configPath = options.config ? options.config : './servr.json';
  if (options.help) {
    // tslint:disable
    return console.log(optionator.generateHelp());
    // tslint:enable
  }
  const pathh = path.resolve(process.cwd(), configPath);
  if (fs.existsSync(pathh)) {
    if (fs.lstatSync(pathh).isDirectory()) {
      Logger.error(
        'The path to the configuration file you specified appears to be a directory. A JSON file was expected.'
      );
    } else {
      try {
        const config = await import(pathh);
        Servr.run(config);
      } catch (e) {
        Logger.error(
          'Failed to import the specified configuration. Something is probably wrong with the JSON configuration - try validating it.'
        );
      }
    }
  } else {
    Logger.error(`The path to the configuration file you specified (${pathh}) does not exist`);
  }
}

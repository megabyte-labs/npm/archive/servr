import { validate } from 'class-validator';
// import { removeSync } from 'fs-extra';
import { Logger } from './lib/log';
import { generateJujuFiles } from './lib/maas-openstack';
import { MAASOpenStackOptions } from './models/maas-openstack-options.model';
import { ServrOptions } from './models/servr-options.model';
import { execSync } from 'child_process';

/**
 * This class exposes the base API for Servr. In general, you should be
 * using this as the entry point.
 *
 * Example usage:
 * ```
 * const config = await import('./servr.json');
 * Servr.run(config);
 * ```
 */
export class Servr {
  /**
   * This method runs through all the tasks that are provided by Servr.
   *
   * @param data Options in the form of [[ServrOptions]]
   * @returns A void Promise
   */
  public static async run(data: ServrOptions): Promise<void> {
    const config = new ServrOptions(data);
    const errors = await validate(config);
    if (errors && errors.length > 0) {
      Logger.error(errors);
    } else {
      // ServrOptions is valid
      if (config.maas) {
        await Servr.maas(config.maas);
      }
    }
  }

  /**
   * Installs Juju, configures MAAS to use it, and deploys OpenStack with Juju
   *
   * @param data Options in the form of [[MAASOpenStackOptions]]
   * @returns A void Promise
   */
  public static async maas(data: MAASOpenStackOptions): Promise<void> {
    try {
      Logger.info('Generating Juju initialization files');
      await generateJujuFiles(data);
      Logger.info('Configuring Juju and the Juju MAAS controller');
      try {
        execSync(`maas login ${data.admin} http://${data.hostname}:5240/MAAS/ ${data.key}`);
        execSync(`maas ${data.admin} dnsresources create fqdn=maas.${data.domain} ip_addresses=${data.ip}`);
        execSync(`maas ${data.admin} dnsresources create fqdn=juju.${data.domain} ip_addresses=${data.ip}`);
      } catch (e) {
        Logger.error('Failed to login and create DNS entries pointing to Juju and MAAS - the entries might already exist', e.toString());
      }
      try {
        require('child_process').execSync('bash /tmp/servr/run.sh', {stdio: 'inherit'});
      } catch (e) {
        Logger.error('Failed to run MAAS/Juju installation script', e.toString());
        throw Error;
      }
      
    } catch (e) {
      Logger.error('Failed to complete MAAS/Juju sequence', e);
    }
    // TODO clean up /tmp/servr
  }
}

{
  "private": false,
  "name": "@woogie/servr",
  "description": "A collection of tools useful for deploying servers/clouds.",
  "license": "MIT",
  "author": "Brian Zalewski <help@megabyte.space> (https://megabyte.space)",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/megabyte-space/modules/servr.git"
  },
  "version": "1.0.1",
  "main": "build/main/index.js",
  "module": "build/module/index.js",
  "bin": {
    "@woogie/servr": "bin/cli",
    "servr": "bin/cli"
  },
  "scripts": {
    "_cov:check": "nyc report && nyc check-coverage --lines 100 --functions 100 --branches 100",
    "_test:unit": "nyc --silent ava",
    "build": "run-s clean && run-p build:*",
    "postbuild": "cp -rf ./src/assets ./build/main && cp -rf ./src/assets ./build/module",
    "build:main": "tsc -p tsconfig.json",
    "build:module": "tsc -p tsconfig.module.json",
    "clean": "trash build test",
    "cov": "run-s build test:unit cov:html && open-cli coverage/index.html",
    "cov:check": "",
    "cov:html": "nyc report --reporter=html",
    "cov:send": "nyc report --reporter=lcov && codecov",
    "describe": "npm-scripts-info",
    "doc": "run-s doc:html && open-cli build/docs/index.html",
    "doc:html": "typedoc src/ --exclude **/*.spec.ts --target esnext --mode file --out build/docs",
    "doc:json": "typedoc src/ --exclude **/*.spec.ts --target ES6 --mode file --json build/docs/typedoc.json",
    "doc:publish": "gh-pages -m \"[ci skip] Updates\" -d build/docs",
    "fix": "run-s fix:*",
    "fix:prettier": "prettier \"src/**/*.ts\" --write",
    "fix:tslint": "tslint --fix --project .",
    "prepare-release": "cp package.json ./src/assets/package.json && run-s reset test cov:check doc:html version doc:publish",
    "reset": "git clean -dfx && git reset --hard && npm i",
    "test": "run-s build test:*",
    "test:lint": "tslint --project . && prettier \"src/**/*.ts\" --list-different",
    "version": "standard-version",
    "watch": "run-s clean build:main && run-p \"build:main -- -w\" \"test:unit -- --watch\""
  },
  "config": {
    "commitizen": {
      "path": "cz-conventional-changelog"
    }
  },
  "typings": "build/main/index.d.ts",
  "dependencies": {
    "@google-cloud/translate": "^5.1.4",
    "class-validator": "^0.11.0",
    "dotenv": "^8.2.0",
    "fs-extra": "^8.1.0",
    "glob": "^7.1.6",
    "handlebars": "^4.7.3",
    "lodash": "^4.17.15",
    "optionator": "^0.8.3",
    "pino": "^5.16.0",
    "pino-pretty": "^3.5.0",
    "update-notifier": "^4.0.0"
  },
  "devDependencies": {
    "@istanbuljs/nyc-config-typescript": "^0.1.3",
    "@types/node": "^13.1.8",
    "ava": "2.2.0",
    "codecov": "^3.5.0",
    "cz-conventional-changelog": "^2.1.0",
    "gh-pages": "^2.0.1",
    "husky": "^4.2.1",
    "lint-staged": "^10.0.2",
    "npm-run-all": "^4.1.5",
    "npm-scripts-info": "^0.3.9",
    "nyc": "^14.1.1",
    "open-cli": "^5.0.0",
    "prettier": "^1.18.2",
    "prettier-package-json": "^2.1.3",
    "standard-version": "^6.0.1",
    "trash-cli": "^3.0.0",
    "tslint": "^5.18.0",
    "tslint-config-prettier": "^1.18.0",
    "tslint-immutable": "^6.0.1",
    "typedoc": "^0.16.8",
    "typescript": "^3.5.3"
  },
  "keywords": [
    "automatic",
    "compression",
    "file",
    "generator",
    "google",
    "image",
    "static",
    "translations"
  ],
  "engines": {
    "node": ">=8.9"
  },
  "publishConfig": {
    "access": "public"
  },
  "ava": {
    "failFast": true,
    "files": [
      "build/main/**/*.spec.js"
    ],
    "sources": [
      "build/main/**/*.js"
    ]
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    ".{ts|js}": [
      "tslint",
      "prettier --write"
    ],
    "package.json": [
      "prettier-package-json --write"
    ]
  },
  "nyc": {
    "extends": "@istanbuljs/nyc-config-typescript",
    "exclude": [
      "**/*.spec.js"
    ]
  },
  "prettier": {
    "bracketSpacing": true,
    "printWidth": 120,
    "requirePragma": false,
    "semi": true,
    "singleQuote": true,
    "tabWidth": 2,
    "trailingComma": "none",
    "useTabs": false
  },
  "scripts-info": {
    "info": "Display information about the package scripts",
    "build": "Clean and rebuild the project",
    "fix": "Try to automatically fix any linting problems",
    "test": "Lint and unit test the project",
    "watch": "Watch and rebuild the project on save, then rerun relevant tests",
    "cov": "Rebuild, run tests, then create and open the coverage report",
    "doc": "Generate HTML API documentation and open it in a browser",
    "doc:json": "Generate API documentation in typedoc JSON format",
    "version": "Bump package.json version, update CHANGELOG.md, tag release",
    "reset": "Delete all untracked files and reset the repo to the last commit",
    "prepare-release": "One-step: clean, build, test, publish docs, and prep a release"
  }
}

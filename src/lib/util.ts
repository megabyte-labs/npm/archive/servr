import * as fs from 'fs';
import glob from 'glob';
import * as Handlebars from 'handlebars';
import { isEqual, isObject, transform } from 'lodash';
import * as path from 'path';

/**
 * Recursively creates a directory
 *
 * @param path The path of the directory tree you want to create
 * @returns A promise that resolves when the operation is complete
 */
export async function createDir(pathh: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.mkdir(pathh, { recursive: true }, error => {
      if (error) {
        reject('Failed to create directory ' + pathh);
      } else {
        resolve();
      }
    });
  });
}

/**
 * Compares two objects
 *
 * @param base The base object
 * @param obj The object you want to find the missing key/values of
 * @returns An object with the key/values that the obj is missing to be of base type
 */
export function diffObjects(base: {}, obj: {}): {} {
  return transform(obj, (result, value, key) => {
    if (!isEqual(value, base[key])) {
      if (!base[key]) {
        // tslint:disable
        result[key] = isObject(value) && isObject(base[key]) ? diffObjects(value, base[key]) : value;
        // tslint:enable
      }
    }
  });
}

/**
 * Return an array of files in a directory
 *
 * @param path The path to scan for files
 * @returns A promise that resolves an array of files
 */
export async function dirFiles(pathh: string): Promise<readonly string[]> {
  return new Promise((resolve, reject) => {
    fs.readdir(pathh, (error, files) => {
      error ? reject(error) : resolve(files.map(x => path.resolve(`${pathh}/${x}`)));
    });
  });
}

/**
 * Executes a command synchronously
 *
 * @param command Command to synchronously run with bash
 */
export function execSync(command: string): string {
  return require('child_process').execSync(command);
}

/**
 * Uses a glob pattern to find matching files
 *
 * @param pattern The pattern to match
 * @returns An array of files that match
 */
export async function globber(pattern: string): Promise<readonly string[]> {
  return new Promise((resolve, reject) => {
    glob(pattern, {}, (error, files) => {
      error ? reject(error) : resolve(files);
    });
  });
}

/**
 * Generates and saves a file given a [Handlebars](https://handlebarsjs.com/)
 * file and a context.
 *
 * @param source The source handlebars file
 * @param target The file to save to
 * @param context The context used to generate the target file given the source
 */
export async function saveHandlebars(source: string, target: string, context: object = {}): Promise<void> {
  return new Promise(async (resolve, reject) => {
    try {
      const src = fs.readFileSync(source).toString();
      const template = Handlebars.compile(src);
      const output = template(context);
      await writeFile(target, output);
      resolve();
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Determines whether the file is valid (whether it exists or not for now)
 *
 * @param pathh The path to the file
 * @returns True if the file is valid
 */
export function validFile(pathh: string): boolean {
  return fs.existsSync(pathh);
}

/**
 * Saves a file
 *
 * @param pathh The absolute path to the file
 * @param data The data being written to the file
 * @returns A promise that resolves if the file was written without error
 */
export async function writeFile(pathh: string, data: any): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.writeFile(pathh, data, error => {
      if (error) {
        reject('Failed to save ' + pathh);
      } else {
        resolve();
      }
    });
  });
}

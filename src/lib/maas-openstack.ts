import { removeSync } from 'fs-extra';
import * as path from 'path';
import { MAASOpenStackOptions } from '../models/maas-openstack-options.model';
import { Logger } from './log';
import { createDir, saveHandlebars } from './util';

export const TMP_DIR = '/tmp/servr';

/**
 * Generates the files necessary to deploy the Juju controller along with
 * an nginx configuration for MAAS and Juju Web UI
 *
 * @param data The options in the form of [[MAASOpenStackOptions]]
 */
export async function generateJujuFiles(data: MAASOpenStackOptions): Promise<void> {
  try {
    const context = { ...data };
    await createDir(TMP_DIR);
    await Promise.all([
      generateFile('maas-cloud.yaml.handlebars', context),
      generateFile('maas-creds.yaml.handlebars', context),
      generateFile('run.sh.handlebars', context),
      generateFile('maas.conf.handlebars', context),
      generateFile('juju.conf.handlebars', context)
    ]);
  } catch (e) {
    Logger.error('Failed to generate Juju files', e);
    removeSync(TMP_DIR);
    throw Error;
  }
}

/**
 * A helper function for [[generateJujuFiles]] that generates each required file
 *
 * @param file The filename of the Juju file from the assets folder
 * @param context The context used to generate the file with Handlebars in the form of [[MAASOpenStackOptions]]
 */
async function generateFile(file: string, context: MAASOpenStackOptions): Promise<void> {
  const source = path.join(__dirname, '../assets/' + file);
  const target = `${TMP_DIR}/${file.replace('.handlebars', '')}`;
  await saveHandlebars(source, target, context);
}
